/**
 * @Author Dale Boisjoli
 * Version 1.0
 *
 * La toile sur le quel les dessins devraient être mis
 */

import javax.swing.*;
import java.awt.*;

public class Toile extends JPanel {

    public Toile() {
        super();
    }
    @Override

    /**
     * Permet de dessiner sur la toile
     */
    public void paintComponent(Graphics graphique){
        super.paintComponent(graphique);
        graphique.setColor(Color.ORANGE);
        graphique.drawLine(20, 98, 75, 98);
        graphique.setColor(Color.cyan);
        graphique.drawRect(100, 120, 150, 120);
        graphique.fillRect(300, 500, 400, 500);
    }


}
