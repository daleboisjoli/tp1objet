/**
 * @Author Dale Boisjoli
 * Version 1.0
 *
 * La classe Menu créer l'objet Menu
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu extends JPanel implements ActionListener {

    String choix;
    ControleurMenu controleurMenu = new ControleurMenu();

    /**
     * Obtient le JFrame
     * @return fenetre
     */
    public JFrame getFenetre() {
        return fenetre;
    }

    /**
     * Obtient le choix de forme
     * @return choix
     */
    public String getChoix() {
        return choix;
    }

    /**
     * Set la fenetre
     * @param fenetre
     */
    public void setFenetre(JFrame fenetre) {
        this.fenetre = fenetre;
    }

    /**
     * Obtient des boutons
     * @return bouton
     */
    public JButton getBouton() {
        return bouton;
    }

    /**
     * Modifie les boutons
     * @param bouton
     */
    public void setBouton(JButton bouton) {
        this.bouton = bouton;
    }

    /**
     * Obtient le choix de forme à effacer
     * @return choixEffacer
     */
    public String[] getChoixEffacer() {
        return choixEffacer;
    }

    /**
     * détermine le choix à effacer
     * @param choixEffacer
     */
    public void setChoixEffacer(String[] choixEffacer) {
        this.choixEffacer = choixEffacer;
    }

    /**
     * Obtient la liste des choix à effacer
     * @return listeChoixEffacer
     */
    public JComboBox<String> getListeChoixEffacer() {
        return listeChoixEffacer;
    }

    /**
     * détermine les éléments dans la liste des choix à effacer
     * @param listeChoixEffacer
     */
    public void setListeChoixEffacer(JComboBox<String> listeChoixEffacer) {
        this.listeChoixEffacer = listeChoixEffacer;
    }

    /**
     * Obtient la liste de choix de forme
     * @return choixForme
     */
    public String[] getChoixForme() {
        return choixForme;
    }

    /**
     * Modifie la liste de choix de forme
     * @param choixForme
     */
    public void setChoixForme(String[] choixForme) {
        this.choixForme = choixForme;
    }

    /**
     * Obtient la liste de choix
     * @return listeChoix
     */
    public JComboBox<String> getListeChoix() {
        return listeChoix;
    }

    private JFrame fenetre = new JFrame();
    private JButton bouton = new JButton();
    private String[] choixEffacer = {};
    private JComboBox<String> listeChoixEffacer = new JComboBox<String>(choixEffacer);
    private String[] choixForme = {"Ligne", "Carré", "Rectangle", "Cercle", "Triangle", "Ellipse"};
    private final JComboBox<String> listeChoix = new JComboBox<String>(choixForme);

    /**
     * Constructeur de menu
     */
    public Menu() {
        setAlignmentX(Component.LEFT_ALIGNMENT);
        setPreferredSize(new Dimension(200, 1100));
        setMaximumSize(new Dimension(200, 1100));
        setBorder(BorderFactory.createTitledBorder("Menu"));
        bouton = new JButton("Dessiner");
        add(bouton);
        bouton.addActionListener(new BoutonDessinerActionListener());
        bouton = new JButton("Effacer");
        bouton.addActionListener(new BoutonEffacerActionListener());
        add(bouton);
        fenetre.pack();
        setVisible(true);
    }

    /**
     * Confirme que le bouton OK fonctionne
     * @param aEvenement the event to be processed
     */
    @Override
    public void actionPerformed(ActionEvent aEvenement) {
        JOptionPane.showMessageDialog(null,
                "Confirmé");
    }

    /**
     * Lorsqu'on appuie sur le bouton Dessiner, fait apparaître le menu dessiner
     */
    private class BoutonDessinerActionListener implements ActionListener {
        public void actionPerformed(ActionEvent aEvenement) {
            removeAll();
            bouton = new JButton("Dessiner");
            add(bouton);
            bouton.addActionListener(new BoutonDessinerActionListener());
            bouton = new JButton("Effacer");
            bouton.addActionListener(new BoutonEffacerActionListener());
            add(bouton);
            menuDessiner();
            controleurMenu.testChoix();
        }
    }
    /**
     * Classe permettant d'implémenter l'événement du bouton Effacer.
     */
    private class BoutonEffacerActionListener implements ActionListener {

        /**
         * Lorsqu'on appuie sur le bouton Effacer, fait apparaître le menu effacer.
         *
         * @param aEvenement L'évenement qui se produit lorsqu'on appuie sur le bouton Effacer
         */
        public void actionPerformed(ActionEvent aEvenement) {
            removeAll();
            bouton = new JButton("Dessiner");
            add(bouton);
            bouton.addActionListener(new BoutonDessinerActionListener());
            bouton = new JButton("Effacer");
            bouton.addActionListener(new BoutonEffacerActionListener());
            add(bouton);
            menuEffacer();
        }
    }

    /**
     * Menu affiché lorsque le bouton Dessiner est appuyé
     */
    public void menuDessiner() {
        add(listeChoix);
        JButton boutonOk = new JButton("OK");
        boutonOk.addActionListener(this);
        add(boutonOk);
        fenetre.pack();
        setVisible(true);
        revalidate();
        repaint();
    }

    /**
     * Menu affiché lorsque le bouton Effacer est appuyé
     */
    public void menuEffacer() {
        add(listeChoixEffacer);
        JButton bouton = new JButton("OK");
        bouton.addActionListener(this);
        add(bouton);
        fenetre.pack();
        setVisible(true);
        revalidate();
        repaint();
    }

    /**
     * Détermine quelle forme a été choisi
     *
     * @return
     */
    public void choisirForme() {
        listeChoix.addActionListener(new choixActionListener());
    }

    /**
     * Écoute la JComboBox et assigne la variable choix
     */
    private class choixActionListener implements ActionListener {
    public void actionPerformed(ActionEvent aEvenement) {
        choix = listeChoix.getSelectedItem().toString();
    }
    }
}
