/**
 * @Author Dale Boisjoli
 * Version 1.0
 *
 * Main, lancement du programme
 */

import javax.swing.*;


public class Programme {

    public static void main(String[] args) {
        Menu Menu = new Menu(); // Créer l'objet menu
        Toile toile = new Toile(); //Créer la toile pour dessiner
        VueMenu vueMenu = new VueMenu();
        JFrame fenetre = new JFrame("Programme Dessin");
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.setLayout(new BoxLayout(fenetre.getContentPane(), BoxLayout.LINE_AXIS));
        fenetre.setSize(550, 300);
        fenetre.getContentPane().add(Menu);
        fenetre.getContentPane().add(toile);
        fenetre.pack();
        fenetre.setVisible(true);

    }
}
