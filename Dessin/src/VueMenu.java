/**
 * @Author Dale Boisjoli
 * Version 1.0
 *
 * La classe VueMenu devrait être la vue du menu
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class VueMenu {

    ControleurMenu controleurMenu = new ControleurMenu();
    Toile toile = new Toile();
    Menu menu = new Menu();
    private JFrame fenetre = new JFrame();
    private JButton bouton = new JButton();
}
